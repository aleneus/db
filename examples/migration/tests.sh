#!/bin/bash

function createDb {
    # $1 - script filename
    # $2 - database filename
    sqlite3 $2 < $1
}

function backupDb {
    # $1 - database filename
    # $2 - version
    cp "$1" "$1.v$2.bak"
}

function migrate {
    # $1 - database filename
    # $2 - migration script filename
    sqlite3 "$1" < "$2"
}

function doSQL {
    # $1 - query
    # $2 - database filename
    echo "$1" | sqlite3 "$2"
}

function printTestName {
    echo ""
    echo "== TEST: $1"
}

function assert {
    echo "ASSERT: $1"
}

function setUp {
    mkdir -p "tmp"
}

function tearDown {
    rm -rf tmp
}

function test1to2 {
    setUp
    printTestName "migrate from version 1 to version 2"

    createDb sql/create/1.sql tmp/db.sqlite

    backupDb tmp/db.sqlite 1
    for ver in 2; do
	migrate tmp/db.sqlite "sql/migrate/$ver.sql"
    done

    doSQL "INSERT INTO Buildings VALUES (4, 40, 3)" tmp/db.sqlite
    doSQL "SELECT * FROM Buildings ORDER BY ID DESC LIMIT 1;" tmp/db.sqlite
    assert "4|40|3"

    doSQL "SELECT * FROM Version;" tmp/db.sqlite
    assert "2"

    tearDown
}

function test2to3 {
    printTestName "migrate from version 2 to version 3"
    setUp

    createDb sql/create/2.sql tmp/db.sqlite

    backupDb tmp/db.sqlite 2
    for ver in 3; do
	migrate tmp/db.sqlite "sql/migrate/$ver.sql"
    done

    doSQL "INSERT INTO Buildings VALUES (4, 40, 3)" tmp/db.sqlite
    doSQL "SELECT count(doors_number) FROM Buildings;" tmp/db.sqlite
    assert "1"

    tearDown
}

function test3to4 {
    printTestName "migrate from version 3 to version 4"
    setUp

    createDb sql/create/3.sql tmp/db.sqlite

    backupDb tmp/db.sqlite 3
    for ver in 4; do
	migrate tmp/db.sqlite "sql/migrate/$ver.sql"
    done

    doSQL ".tables" tmp/db.sqlite
    assert "Buildings, Cities, Version"

    tearDown
}


function test2to4 {
    printTestName "migrate from version 2 to version 4"
    setUp

    createDb sql/create/2.sql tmp/db.sqlite

    backupDb tmp/db.sqlite 2
    for ver in 3 4; do
	migrate tmp/db.sqlite "sql/migrate/$ver.sql"
    done

    doSQL ".tables" tmp/db.sqlite
    assert "Buildings, Cities, Version"

    tearDown
}


function test1to4 {
    printTestName "migrate from version 1 to version 4"
    setUp

    createDb sql/create/1.sql tmp/db.sqlite

    backupDb tmp/db.sqlite 1
    for ver in 2 3 4; do
	migrate tmp/db.sqlite "sql/migrate/$ver.sql"
    done

    doSQL ".tables" tmp/db.sqlite
    assert "Buildings, Cities, Version"

    tearDown
}


G_REPORT=""
G_SUCCESS="Ok"

function run {
    $1

    read -p "y/n: " ok
    if [ "$ok" = "y" ]; then
	G_REPORT+="."
    else
	G_REPORT+="E"
	G_SUCCESS="Failed"
    fi
}

run test1to2
run test2to3
run test3to4
run test2to4
run test1to4

echo ""
echo "$G_REPORT"
echo "$G_SUCCESS"
