-- create test database of version 1
DROP TABLE IF EXISTS Version;
CREATE TABLE Version (version INTEGER);
INSERT INTO Version VALUES (1);

CREATE TABLE Buildings(ID INTEGER PRIMARY KEY, height INTEGER);
