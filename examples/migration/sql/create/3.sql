-- create test database of version 3
DROP TABLE IF EXISTS Version;
CREATE TABLE Version (version INTEGER);
INSERT INTO Version VALUES (2);

CREATE TABLE Buildings(ID INTEGER PRIMARY KEY, height INTEGER, doors_number INTEGER);
