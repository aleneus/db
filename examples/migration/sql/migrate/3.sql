-- migration from version 2 to 3

BEGIN TRANSACTION;

ALTER TABLE Buildings RENAME TO tmp_Buildings;

CREATE TABLE Buildings(
       ID INTEGER PRIMARY KEY,
       height INTEGER,
       doors_number INTEGER
);

INSERT INTO Buildings(ID, height, doors_number)
SELECT ID, height, doors
FROM tmp_Buildings;

DROP TABLE tmp_Buildings;

DROP TABLE IF EXISTS Version;
CREATE TABLE Version (version INTEGER);
INSERT INTO Version VALUES (3);

END TRANSACTION;
