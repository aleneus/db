import sqlite3


class SQLiteTestHelper:
    def __init__(self):
        self.__conn = sqlite3.connect(":memory:")
        self.__cur = self.__conn.cursor()

    def __del__(self):
        self.__conn.close()

    def do_create(self, query):
        self.__cur.execute(query)

    def do_create_script(self, text):
        parts = text.split(";")
        for part in parts:
            if "CREATE" in part:
                self.do_create(part)

    def do_file(self, fname):
        with open("schema.sql") as buf:
            text = buf.read()
        self.do_create_script(text)

    def do_insert(self, query):
        self.__cur.execute(query)
        self.__conn.commit()

    def do_select(self, query):
        res = self.__cur.execute(query)
        return list(res)
