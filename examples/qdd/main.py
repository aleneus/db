import unittest
from sqltest import SQLiteTestHelper


class TestPoints(unittest.TestCase):
    def __init__(self, *args):
        super().__init__(*args)
        self.helper = None

    def setUp(self):
        self.helper = SQLiteTestHelper()
        self.helper.do_file("schema.sql")

    def test_output_all_points(self):
        self.helper.do_insert("INSERT INTO T(x, y) VALUES (1, 2);")
        self.helper.do_insert("INSERT INTO T(x, y) VALUES (3, 4);")

        r = self.helper.do_select("SELECT * FROM  T;")
        self.assertEqual(r, [(1, 2), (3, 4)])

    def test_output_all_trianles(self):
        self.helper.do_insert("INSERT INTO Triangles(id) VALUES (1);")
        r = self.helper.do_select("SELECT * FROM  Triangles;")
        self.assertEqual(r, [(1,)])


unittest.main()
